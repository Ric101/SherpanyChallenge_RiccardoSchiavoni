import json
from django.conf import settings
from django.shortcuts import render
from django.http import JsonResponse
from .models import Location
from .tools.CloudTables import CloudTableManager


def home(request):
    return render(request, "home.html", {})


# AJAX (includes business logic, as project is simple enough)
def addLocation(request):
    locationData = json.loads(request.POST.get("location"))
    tableManager = CloudTableManager(settings.GOOGLE_FUSIONTABLES__TABLE_ID["location"])

    location, created = Location.getOrCreateAvoidingDuplicateAddresses(
        latitude=locationData["latitude"],
        longitude=locationData["longitude"],
        address=locationData["address"]
    )

    if created:
        try:
            # assuming tale in FusionTables has same structure as DB
            tableManager.insertRow(location.toCloudTableSerializableDict())
        except:
            location.delete()
            raise
            return JsonResponse({"status": "error"})

    return JsonResponse({
        "status": "ok",
        "location": location.toSerializableDict(),
        "created": created
    })


def clearAllLocations(request):
    tableManager = CloudTableManager(settings.GOOGLE_FUSIONTABLES__TABLE_ID["location"])

    try:
        tableManager.clearTable()
    except:
        return JsonResponse({"status": "error"})

    Location.objects.all().delete()
    return JsonResponse({"status": "ok"})

