import os
import requests
from django.conf import settings
from oauth2client.service_account import ServiceAccountCredentials

from googleapiclient.discovery import build


class CloudTableManager:
    """
    Wrapper for managing google Fusion tables.
    """

    def __init__(self, table_id=None, ):
        """
        hardcoded values: only changes if there a change in Fusion Tables API,
        project setup, or we'll decide to move to a different service. In all
        cases this wrapper class will need refactoring
        """

        scopes = ['https://www.googleapis.com/auth/fusiontables']
        credentials = ServiceAccountCredentials.from_json_keyfile_name(
            os.path.join(settings.BASE_DIR, "resources", "SherpanyChallenge17-2751d9fb0fc2.json"),
            scopes=scopes)

        self.tableId = table_id
        self.tablesManager = build('fusiontables', 'v2', credentials=credentials)

    def insertRow(self, data):
        """
        :param data: dict {columnName: value}
        """
        orderedColumnNames = list(data.keys())

        sql = "INSERT INTO {tableId} ({columnNames}) VALUES ({values})"\
            .format(
                tableId=self.tableId,
                columnNames=", ".join(orderedColumnNames),
                values=", ".join([str(data[col]) for col in orderedColumnNames])
            )

        print("sql: " + sql)
        self.tablesManager.query().sql(sql=sql).execute()

    def readAllTable(self):
        sql = "SELECT * FROM {tableId}"\
            .format(tableId=self.tableId)

        readTable = self.tablesManager.query().sql(sql=sql).execute()
        return readTable

    def clearTable(self):
        sql = "DELETE FROM {tableId}".format(tableId=self.tableId)
        self.tablesManager.query().sql(sql=sql).execute()

