import random
from django.test import TestCase
from .tools.CloudTables import CloudTableManager
from django.conf import settings


class TestCloudTableManager(TestCase):

    def setUp(self):
        tableId = settings.GOOGLE_FUSIONTABLES__TABLE_ID["location"]
        self.locationTableManager = CloudTableManager(tableId)

    def testTableManager(self):
        nRows = lambda data: len(data["rows"]) if "rows" in data else 0

        latitude = random.randint(-18000, 18000) / 100.0
        longitude = random.randint(-18000, 18000) / 100.0
        data = {
            "latitude": latitude,
            "longitude": longitude,
            "address": "'lacacca'",
        }

        readTable = self.locationTableManager.readAllTable()
        tableLength_before = nRows(readTable)

        self.locationTableManager.insertRow(data)

        readTable = self.locationTableManager.readAllTable()
        tableLength_after = nRows(readTable)
        lastRow = readTable["rows"][-1]

        assert tableLength_before == tableLength_after - 1
        assert float(lastRow[0]) == latitude
        assert float(lastRow[1]) == longitude

        self.locationTableManager.clearTable()
        readTable = self.locationTableManager.readAllTable()
        assert nRows(readTable) == 0
