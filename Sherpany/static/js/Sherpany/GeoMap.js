class Location {
    /* Wrapper for google's LatLng objects - to decouple from Google's API */

    constructor(latitude, longitude, address) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address; // might be missing
    }

    getPromiseOfAddressAsString() {
        let geocoder = Location.getGeocoder();

        return new Promise((resolve, reject) => {
            let geocodeResultHandler = (results, status) => {
                if (status === 'OK' && results[0]) {
                    let address = results[0].formatted_address;
                    this.address = address;
                    resolve(address);
                }
                else {
                    reject();
                }
            };

            geocoder.geocode(
                {'location': Location.convert.to(this)},
                geocodeResultHandler
            );
        });
    }

    toSerializableObject() {
        return {
            latitude: this.latitude,
            longitude: this.longitude,
            address: this.address,
        }
    }
}

Location.getGeocoder = () => {
    if (Location._geocoder === undefined)
        Location._geocoder = new google.maps.Geocoder;

    return Location._geocoder;
};

Location.convert = {
    /*
    * convert to and from equivalent element in API in use
    * (in this case Google maps API)
    * */

    from: (lat_lng) => {
        return new Location(lat_lng.lat(), lat_lng.lng());
    },

    to: (location) => {
        return new google.maps.LatLng(location.latitude, location.longitude);
    }

};


class GeoMap {
    /* Wrapper for google's Map objects - to decouple from Google's API */

    constructor($node) {
        /*
        * $node: jQuery instance of DOM element to use for the map
        * */
        this.$ = $node;
        this.$.addClass("GeoMap");

        let dom_node = this.$[0];
        this._map = new google.maps.Map(dom_node, {
            zoom: 2,
            center: {lat: 0, lng: 0}
        });

        this._onClickListener = this._map.addListener("click", (e) => {
            this._mapClick(Location.convert.from(e.latLng));
        });

        this._handlers = {
            click: [],
        };

        this._overlays = {
            markers: [],
        };
    }

    addOnClickHandler(f) {
        /*
        * f fill receive 2 arguments:
        *           this
        *           {latitude: y, longitude: x}
        * */
        if (!$.isFunction(f))
            throw new Error("onClick handler for Map must be a function");

        if (this._handlers.click.includes(f)) {
            console.warn("Careful, trying to add handler more than once to same Map");
            return;
        }

        this._handlers.click.push(f);
    }

    clearAllOnClickHandlers() {
        google.maps.event.clearListeners(this._map, "click");
        this._handlers.click.functions = [];
        this._handlers.click.listeners = [];
    }

    addMarker(location) {
        let latLng = Location.convert.to(location);

        let marker = new google.maps.Marker({
            position: latLng,
            map: this._map
        });

        this._overlays.markers.push(marker);
    }

    clearAllMarkers() {
        for (let marker of this._overlays.markers)
            this._removeMarker(marker);

        this._overlays.markers = [];
    }

    _removeMarker(marker) {
        marker.setMap(null);
    }

    _mapClick(location) {
        for (let handler of this._handlers.click)
            handler(this, location);
    }

}
