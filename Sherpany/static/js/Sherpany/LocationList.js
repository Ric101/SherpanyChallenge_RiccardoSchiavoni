class LocationWidget {
    /*
    * as a widget, it creates its own (jQuery element of a) div.
    * Used by LocationList.
    * */

    constructor(location) {
        this.$ = $("<div></div>");
        this.$.addClass("LocationWidget");

        this.location = location;

        this.location.getPromiseOfAddressAsString().then((address) => {
            console.log(address);
            this.$.text("" +
                this.location.latitude + " : " +
                this.location.longitude + " -> " +
                this.location.address
            );

        });
    }
}

class LocationList {
    /*
    * Encapsulates functions for managing the locations list.
    * Uses (jQuery element of) DOM node passed as argument
    * */

    constructor($node) {
        /*
        * $node: jQuery instance of DOM element to use for the LocationsList
        * */
        this.$ = $node;
        this.$.addClass("LocationsList");

        this.locations = [];
    }

    addLocation(location) {
        let locationWidget = new LocationWidget(location);
        this.$.prepend(locationWidget.$);
        this.locations.splice(0, 0, locationWidget);
    }

    clear() {
        this.$.empty();
        this.locations = [];
    }

}