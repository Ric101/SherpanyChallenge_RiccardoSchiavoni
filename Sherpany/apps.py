from django.apps import AppConfig


class SherpanyConfig(AppConfig):
    name = 'Sherpany'
