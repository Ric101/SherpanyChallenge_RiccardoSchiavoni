from django.db import models


class Location(models.Model):

    latitude = models.DecimalField(max_digits=9, decimal_places=6)
    longitude = models.DecimalField(max_digits=9, decimal_places=6)
    address = models.CharField(max_length=1024)

    def toSerializableDict(self):
        # could also use django serializer, but is nice to have control of this

        return {
            "id": self.id,
            "latitude": self.latitude,
            "longitude": self.longitude,
            "address": self.address,
        }

    def toCloudTableSerializableDict(self):
        # format as needed to create cloud tables entries

        return {
            "latitude": self.latitude,
            "longitude": self.longitude,
            "address": "'" + self.address.replace("'", "\'") + "'",
        }

    @classmethod
    def getOrCreateAvoidingDuplicateAddresses(cls, latitude, longitude, address):
        location, created = Location.objects.get_or_create(
            address=address,
            defaults={
                "latitude": latitude,
                "longitude": longitude,
            }
        )

        return location, created
