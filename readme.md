### Functionality

User can click anywhere on the map
 - if location does not have a valid address, it is ignored (+ notification to console)
 - if the location is valid, it is sent to the server, the server checks if the ADDRESS already exists and, only if it does not, if creates a new entry and tries to add the location to the Fusion Table.
    - if the storing is successful, a marker is added on the map at the clicked location, and the location is added at the top of the bottom's panel
    - if the storing fails, the db entry is also removed, and the calls fails

User can click the "clear all" button
  - all entries are deleted from both the db and Fusion Table
  - if clearing is successful, the map and addresses's pane are cleared


## Notes and limitations
  - non ASCII chars in addresses are not explicitly supported
  - there is not support for translation
  - it is designed for a single user a time
    - clearing removes ALL entries
    - 2 users at the same time cannot use the same address
    - no information is stored on the server, not on the Fusion Table, which identifies the source of the locations
  - markers appear with a delay, after the response from the server