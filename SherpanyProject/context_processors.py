from django.conf import settings


def import_settings(request):
    return {"settings": {
        "VERSION": settings.VERSION,
        "DEBUG": settings.DEBUG,
        "GOOGLE_MAPS__API_KEY": settings.GOOGLE_MAPS__API_KEY,
    }}
