# from django.contrib import admin
from django.conf.urls import url
import Sherpany.views

urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^$', Sherpany.views.home),

    # AJAX
    url(r'ajax/add_location', Sherpany.views.addLocation),
    url(r'ajax/clear_all_locations', Sherpany.views.clearAllLocations),

]
